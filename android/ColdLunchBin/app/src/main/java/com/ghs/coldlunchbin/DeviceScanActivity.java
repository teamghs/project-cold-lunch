/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ghs.coldlunchbin;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.UUID;

/**
 * Activity for scanning and displaying available Bluetooth LE devices.
 */
public class DeviceScanActivity extends Activity {
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothDevice mDevice;
    private boolean mScanning;
    private boolean isConnecting = false;
    private boolean isWaitingForDeposit = false;
    private boolean disconnecting = false;
    private final Handler mMainThreadHandler = new Handler(Looper.getMainLooper());
    private GpioProcessor mGpioProcessor;
    private GpioProcessor.Gpio mGlassDoor;
    private GpioProcessor.Gpio mMetalDoor;

    private VideoView mTiredVideoView, mHappyVideoView, mSadVideoView, mMovingEyes, mConfused;

    private List<VideoView> mVideoViews;

    private Queue<VideoView> mVideoQueue;

    private static final int REQUEST_ENABLE_BT = 1;
    // Stops scanning after 10 seconds.
    private static final long SCAN_PERIOD = 100000;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    private MediaPlayer.OnCompletionListener mOnCompleteListener;

    private VideoView setupVideoView(String videoName, int id) {
        VideoView videoView = (VideoView)findViewById(id);
        int videoId = getResources().getIdentifier(videoName, "raw", getPackageName());
        final String path = "android.resource://" + getPackageName() + "/" + videoId;

        videoView.setVideoPath(path);

        mVideoViews.add(videoView);

        return videoView;
    }

    private void playVideoView(final VideoView videoView) {
        for (final VideoView view : mVideoViews) {
            if (view != videoView) {
                view.pause();
                view.setOnCompletionListener(null);
            } else {
                view.setVisibility(View.VISIBLE);
                view.start();
                view.setOnCompletionListener(mOnCompleteListener);
                view.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        view.setOnPreparedListener(null);

                        for (VideoView view : mVideoViews) {
                            if (view != videoView) {
                                view.seekTo(0);
                                view.setVisibility(View.GONE);
                            }
                        }
                    }
                });
            }
        }
    }

    private void queueVideoView(final VideoView videoView) {
        mMainThreadHandler.post(new Runnable() {
            public void run() {
                playVideoView(videoView);
//                mVideoQueue.add(videoView);
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_device_scan);

        mVideoViews = new ArrayList<>();
        mVideoQueue = new LinkedList<>();

        mTiredVideoView = setupVideoView("tired", R.id.tired_video_view);
        mHappyVideoView = setupVideoView("happy", R.id.happy_video_view);
        mSadVideoView = setupVideoView("sad", R.id.sad_video_view);
        mMovingEyes = setupVideoView("moving_eyes", R.id.moving_eyes_video_view);
        mConfused = setupVideoView("confused", R.id.confused_video_view);

        mOnCompleteListener = new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                mMainThreadHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (mVideoQueue.size() > 0) {
                            VideoView videoView = mVideoQueue.remove();
                            playVideoView(videoView);
                        } else {
                            playVideoView(mTiredVideoView);
                        }
                    }
                });
            }
        };

        playVideoView(mTiredVideoView);

        // Use this check to determine whether BLE is supported on the device.  Then you can
        // selectively disable BLE-related features.
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }

        mGpioProcessor = new GpioProcessor();
        mGlassDoor = mGpioProcessor.getPin23();
        mMetalDoor = mGpioProcessor.getPin25();
        mGlassDoor.out();
        mGlassDoor.high();
        mMetalDoor.out();
        mMetalDoor.high();

        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        System.loadLibrary("samples-paj7620");
    }

    private void startSensorPolling() {
        mMainThreadHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                pollSensor();
            }
        }, 2000);
    }

    Runnable mPollRunnable = new Runnable() {
        @Override
        public void run() {
            pollSensor();
        }
    };

    private void pollSensor() {
        String gesture = getGesture();
        Log.d("Wooly", "Gesture: " + gesture);
        if (isWaitingForDeposit) {
            if (!gesture.startsWith("waiting")) {
                sendDeposit();
            }
        }

        mMainThreadHandler.postDelayed(mPollRunnable, 500);
    }

    private void closeBins() {
        isWaitingForDeposit = false;
        mGlassDoor.high();
        mMetalDoor.high();
    }

    private void sendDeposit() {
        queueVideoView(mConfused);
        mCommunicationCharacteristic.setValue("deposit");
        mGatt.writeCharacteristic(mCommunicationCharacteristic);
    }

    @Override
    protected void onResume() {
        super.onResume();
        startSensor();
        startSensorPolling();

        // Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
        // fire an intent to display a dialog asking the user to grant permission to enable it.
        if (!mBluetoothAdapter.isEnabled()) {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }

        scanLeDevice(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // User chose not to enable Bluetooth.
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            finish();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mScanning) {
            scanLeDevice(false);
        }

        mMainThreadHandler.removeCallbacks(mPollRunnable);

        if (mDevice != null) {
            mGatt.disconnect();
            mGatt.close();
            mDevice = null;
        }
    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            // Stops scanning after a pre-defined scan period.
//            mMainThreadHandler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    mScanning = false;
//                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
//                    invalidateOptionsMenu();
//                }
//            }, SCAN_PERIOD);

            mScanning = true;
            mBluetoothAdapter.startLeScan(mLeScanCallback);
        } else {
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
    }

    private void evaluateDevice(BluetoothDevice device) {
        if (device.getName() != null) {
            Log.d("Wooly", "Device: " + device.getName());
            if (device.getName().contains("ProjectColdLunch")) {
                connect(device);
            }
        }
    }

    protected void connect(BluetoothDevice device) {
        if (device == null) return;
        if (mScanning) {
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
            mScanning = false;
        }

        mDevice = device;
        open();
    }

    private void open() {
        if (isConnecting) {
            return;
        }
        isConnecting = true;
        final Context context = this;
        mMainThreadHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mDevice.connectGatt(context, false, bluetoothGattCallback);
            }
        }, 500);
    }

    private BluetoothGattCharacteristic mCommunicationCharacteristic;

    private BluetoothGatt mGatt;
    private static final int BLE_CONNECTED = 2;
    private static final int BLE_DISCONNECTED = 0;
    private static final int GATT_CLIENT_COULD_NOT_BE_ATTACHED = 133;

    private BluetoothGattCallback bluetoothGattCallback = new BluetoothGattCallback() {

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if (newState == BLE_CONNECTED) {
                if (mGatt != null) {
                    mGatt.disconnect();
                    mGatt.close();
                }
                mGatt = gatt;
                mGatt.discoverServices();
            } else if (newState == BLE_DISCONNECTED) {
                isConnecting = false;
                boolean shouldReconnect = false;
                if (status == GATT_CLIENT_COULD_NOT_BE_ATTACHED) {
                    Log.e("Wooly", "GATT could not be attached. Retrying.");
                    open();
                } else if (disconnecting) {
                    mGatt.close();
                    mMainThreadHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            scanLeDevice(true);
                        }
                    }, 2000);
                } else {
                    open();
                }
            }
        }

        @Override
        public void onServicesDiscovered(final BluetoothGatt gatt, int status) { // iOS setupServicesAndCharacteristics equivalent
            if (!mGatt.equals(gatt)) {
                Log.d("Wooly", "Mismatch GATT");
                return;
            }
            Log.v("Wooly", String.format("onServicesDiscovered count=%d status=%d", gatt.getServices().size(), status));

            UUID serviceUuid = UUID.fromString("E20A39F4-73F5-4BC4-A12F-17D1AD07A960");
            UUID handshakeCharacteristicUUID = UUID.fromString("08590F7E-DB05-467E-8757-72F6FAEB13D4");

            BluetoothGattService comService = null;
            List<BluetoothGattService> services = gatt.getServices();
            for (BluetoothGattService service : services) {
                Log.d("Wooly", "service = " + service.getUuid().toString());
                if (service.getUuid().equals(serviceUuid)) {
                    comService = service;
                    break;
                }
            }

            List<BluetoothGattCharacteristic> characteristics = comService.getCharacteristics();
            for (BluetoothGattCharacteristic characteristic :
                    characteristics) {
                if (characteristic.getUuid().equals(handshakeCharacteristicUUID)) {
                    mCommunicationCharacteristic = characteristic;
                    Log.d("Wooly", "Found handshake characteristic = " + characteristic.getUuid().toString());
                    mCommunicationCharacteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                    break;
                }
                Log.d("Wooly", "characteristic = " + characteristic.getUuid().toString());
            }

            gatt.setCharacteristicNotification(mCommunicationCharacteristic, true);
            UUID uuid = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
            BluetoothGattDescriptor descriptor = mCommunicationCharacteristic.getDescriptor(uuid);
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            gatt.writeDescriptor(descriptor);

            mMainThreadHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mCommunicationCharacteristic.setValue("handshake");
                    gatt.writeCharacteristic(mCommunicationCharacteristic);
                }
            }, 2000);
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);

            if (characteristic.getUuid().equals(mCommunicationCharacteristic.getUuid()) && status == BluetoothGatt.GATT_SUCCESS) {
                Log.d("Wooly", "Write successful");
            } else {
                Log.e("Wooly", "Write failed");
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);

            if (characteristic.getUuid().equals(mCommunicationCharacteristic.getUuid())) {
                String message = characteristic.getStringValue(0);
                Log.d("Wooly", "characteristic changed: " + message);

                if (message.startsWith("open")) {
                    String[] commands = message.split("\\s+");
                    if (commands.length > 1) {
                        openBin(commands[1]);
                    }

                    queueVideoView(mMovingEyes);
                } else if (message.startsWith("close")) {
                    closeBins();
                } else if (message.startsWith("disconnect")) {
                    Log.d("Wooly", "Disconnecting");
                    disconnecting = true;
                    gatt.disconnect();
                    isConnecting = false;

                    queueVideoView(mSadVideoView);
                } else if (message.startsWith("Thumbs Up!")) {
                    queueVideoView(mHappyVideoView);
                }
            }

        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);

            if (characteristic.getUuid().equals(mCommunicationCharacteristic.getUuid())) {
                Log.d("Wooly", "Read com characteristic: " + characteristic.getStringValue(0));
            }
        }
    };

    private void openBin(String binName) {
        isWaitingForDeposit = true;

        if (binName.startsWith("paper")) {
            mGlassDoor.low();
        } else if (binName.startsWith("metal")) {
            mMetalDoor.low();
        }
    }

    // Device scan callback.
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {

                @Override
                public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            evaluateDevice(device);
                        }
                    });
                }
            };

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "DeviceScan Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.ghs.coldlunchbin/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "DeviceScan Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.ghs.coldlunchbin/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    static class ViewHolder {
        TextView deviceName;
        TextView deviceAddress;
    }

    private native void startSensor();
    private native String getGesture();
}