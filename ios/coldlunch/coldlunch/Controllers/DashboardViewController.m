//
//  DashboardViewController.m
//  coldlunch
//
//  Created by Jordan Hesse on 2016-01-02.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import "DashboardViewController.h"
#import "DataRewardController.h"

@implementation DashboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.tabBar setTintColor:[UIColor whiteColor]];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[DataRewardController sharedInstance] showOptInFromViewController:self];
}

@end
