//
//  RecycleViewController.m
//  coldlunch
//
//  Created by Jordan Hesse on 2016-01-02.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import "RecycleViewController.h"

#import "BluetoothManager.h"

@interface RecycleViewController ()

@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (strong, nonatomic) UIViewController *subViewController;

@end

@implementation RecycleViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Remove the old sub view controller
    [self.subViewController willMoveToParentViewController:nil];
    [self.subViewController didMoveToParentViewController:nil];
    [self.subViewController removeFromParentViewController];
    [self.subViewController.view removeFromSuperview];

    // Add the connect view controller
    UIViewController *connectViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Connect"];
    [connectViewController willMoveToParentViewController:self];
    [self addChildViewController:connectViewController];
    [connectViewController didMoveToParentViewController:self];
    self.subViewController = connectViewController;
    [connectViewController.view setFrame:self.containerView.bounds];
    [self.containerView addSubview:connectViewController.view];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[BluetoothManager sharedInstance] disconnectFromDumpster];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    self.subViewController = segue.destinationViewController;
}

- (void)transitionToSubViewController:(UIViewController *)subViewController {
    [subViewController willMoveToParentViewController:self];
    [self addChildViewController:subViewController];
    [self.subViewController willMoveToParentViewController:nil];
    
    [subViewController.view setFrame:self.subViewController.view.frame];
    
    [self transitionFromViewController:self.subViewController
                      toViewController:subViewController
                              duration:0.3
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                            } completion:^(BOOL finished) {
                                [subViewController didMoveToParentViewController:nil];
                                [self.subViewController removeFromParentViewController];
                                [subViewController didMoveToParentViewController:self];
                                self.subViewController = subViewController;
                            }];
    
}

@end
