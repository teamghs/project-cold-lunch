//
//  RecycleNavigationController.m
//  coldlunch
//
//  Created by Jordan Hesse on 2016-01-02.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import "RecycleNavigationController.h"

@interface RecycleNavigationControllerAnimator : NSObject <UIViewControllerInteractiveTransitioning>

@end

@interface RecycleNavigationController () <UINavigationControllerDelegate>

@end

@implementation RecycleNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setDelegate:self];
}

- (id <UIViewControllerInteractiveTransitioning>)navigationController:(UINavigationController *)navigationController
                          interactionControllerForAnimationController:(id <UIViewControllerAnimatedTransitioning>) animationController {
    return [[RecycleNavigationControllerAnimator alloc] init];
}

@end

@implementation RecycleNavigationControllerAnimator

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext {
    return 0.3;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext {
    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    [transitionContext.containerView insertSubview:toViewController.view belowSubview:fromViewController.view];
    [UIView animateWithDuration:[self transitionDuration:transitionContext]
                     animations:^{
                         [fromViewController.view setAlpha:0.0];
                     } completion:^(BOOL finished) {
                         [transitionContext completeTransition:finished];
                     }];
}

@end