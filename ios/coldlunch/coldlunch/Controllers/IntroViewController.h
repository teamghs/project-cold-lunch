//
//  IntroViewController.h
//  coldlunch
//
//  Created by Jordan Hesse on 2016-01-02.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "M2XController.h"

@interface IntroViewController : UIViewController

- (void)resetValuesForDemoWithCompletionHandler:(ModelsRetrievedCompletionBlock)completion;

@end
