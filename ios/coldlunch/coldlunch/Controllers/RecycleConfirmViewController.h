//
//  RecycleConfirmViewController.h
//  coldlunch
//
//  Created by Jordan Hesse on 2016-01-02.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecycleConfirmViewController : UIViewController

@property (strong, nonatomic) NSString *typeParam;

@end
