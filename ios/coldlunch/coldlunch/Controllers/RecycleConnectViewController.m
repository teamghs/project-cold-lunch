//
//  RecycleConnectViewController.m
//  coldlunch
//
//  Created by Jordan Hesse on 2016-01-02.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import "RecycleConnectViewController.h"

#import "SpeechManager.h"
#import "MouthManager.h"
#import "BluetoothManager.h"

#import <Toast/UIView+Toast.h>

@import AVFoundation;

@interface RecycleConnectViewController () <BluetoothDelegate, AVAudioPlayerDelegate>

@property (strong, nonatomic) AVAudioPlayer *audioPlayer;

@end

@implementation RecycleConnectViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[BluetoothManager sharedInstance] setDelegate:self];
    [[BluetoothManager sharedInstance] startAdvertising];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    [self.audioPlayer setDelegate:nil];
    self.audioPlayer = nil;

    [[BluetoothManager sharedInstance] stopAdvertising];
    [[BluetoothManager sharedInstance] setDelegate:nil];
}

- (void)bluetoothManagerDepotConnected {
    [self.view makeToast:@"Waking up..."];
    
    [self playConnectedMessage];
}

- (void)bluetoothManagerDepotDisconnected {
}

- (void)bluetoothManagerItemWasDeposited {
}

- (void)bluetoothManagerDepositTimedOut {
}

- (void)playConnectedMessage {
    NSError *error = nil;
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:YES error:&error];
    if (error) {
        return;
    }

    [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
    if (error) {
        return;
    }
    
    [[SpeechManager sharedInstance] textToSpeech:@"Hey Chris! What do you want to recycle today?"
                                  withCompletion:^(id responseData, NSError *responseError) {
                                      if (responseError) {
                                          [self performSegueWithIdentifier:@"Voice" sender:nil];
                                          return;
                                      }
                                      
                                      NSError *error = nil;
                                      
//                                      NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//                                      NSString *documentsDirectory = [paths objectAtIndex:0];
//                                      NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, @"recording.wav"];
//                                      
//                                      NSData* data = responseData;
//                                      [data writeToFile:filePath atomically:NO];
//
                                      self.audioPlayer = [[AVAudioPlayer alloc] initWithData:responseData error:&error];
                                      if ([self.audioPlayer prepareToPlay])
                                      {
                                          if (error) {
                                              [self performSegueWithIdentifier:@"Voice" sender:nil];
                                              return;
                                          }
                                          
                                          [self.audioPlayer setDelegate:self];
                                          [self.audioPlayer play];
                                          
                                          [[MouthManager sharedInstance] startTalking];
                                      } else {
                                          [[MouthManager sharedInstance] stopTalking];
                                          
                                          [self performSegueWithIdentifier:@"Voice" sender:nil];
                                      }
                                      
                                  }];
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    [[MouthManager sharedInstance] stopTalking];

    [self performSegueWithIdentifier:@"Voice" sender:nil];
}

@end
