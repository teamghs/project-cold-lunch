//
//  RecycleConfirmViewController.m
//  coldlunch
//
//  Created by Jordan Hesse on 2016-01-02.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import "RecycleConfirmViewController.h"
#import "RecycleCompleteViewController.h"
#import "BluetoothManager.h"
#import "M2XController.h"
#import "RecycleSegue.h"

#import <SVProgressHUD/SVProgressHUD.h>

@interface RecycleConfirmViewController () <BluetoothDelegate>

@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) IBOutlet UILabel *typeLabel;

@property (weak, nonatomic) IBOutlet UIView *confirmView;
@property (weak, nonatomic) IBOutlet UIView *loopView;

@property (strong, nonatomic) UIDynamicAnimator *animator;
@property (strong, nonatomic) NSString *binType;

@property (nonatomic) int glassCount;
@property (nonatomic) int plasticCount;
@property (nonatomic) int paperCount;
@property (nonatomic) int metalCount;

@end

@implementation RecycleConfirmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.containerView];
    [self.confirmView setAlpha:1.0];
    [self.loopView setAlpha:0.0];
    
    [self.typeLabel setText:self.typeParam];
    
    CAShapeLayer *circleMask = [CAShapeLayer layer];
    UIBezierPath *circularPath = [UIBezierPath bezierPathWithRoundedRect:self.containerView.bounds
                                                            cornerRadius:CGRectGetWidth(self.containerView.bounds) * 0.5];
    [circleMask setPath:circularPath.CGPath];
    [self.containerView.layer setMask:circleMask];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"CompleteRecycle"]) {
        RecycleCompleteViewController* vc = [segue destinationViewController];
        vc.paperCount = _paperCount;
        vc.metalCount = _metalCount;
        vc.plasticCount = _plasticCount;
        vc.glassCount = _glassCount;
    } else if ([segue isKindOfClass:[RecycleSegue class]]) {
        [[BluetoothManager sharedInstance] closeDumpster];
    }
}

- (IBAction)finishedTapped:(id)sender {
    [[BluetoothManager sharedInstance] closeDumpster];

    if (self.glassCount + self.plasticCount + self.paperCount + self.metalCount == 0) {
        [self performSegueWithIdentifier:@"CompleteRecycle" sender:self];
        return;
    }
    
    [SVProgressHUD showWithStatus:@"Checking your score..."];
    [self updateM2XStreamModel:[[M2XController sharedInstance] glassModel]
                         count:self.glassCount
                withCompletion:^(NSError *error) {
                    [self updateM2XStreamModel:[[M2XController sharedInstance] plasticModel]
                                         count:self.plasticCount
                                withCompletion:^(NSError *error) {
                                    [self updateM2XStreamModel:[[M2XController sharedInstance] paperModel]
                                                         count:self.paperCount
                                                withCompletion:^(NSError *error) {
                                                    [self updateM2XStreamModel:[[M2XController sharedInstance] metalModel]
                                                                         count:self.metalCount
                                                                withCompletion:^(NSError *error) {
                                                                    [[M2XController sharedInstance] populateModelsWithCompletionHandler:^(NSError *error) {
                                                                        [SVProgressHUD dismiss];
                                                                        [self performSegueWithIdentifier:@"CompleteRecycle" sender:self];
                                                                    }];
                                                                }];
                                                }];
                                }];
                }];
}

- (void)updateM2XStreamModel:(M2XStreamModel *)streamModel count:(int)count
              withCompletion:(ModelsRetrievedCompletionBlock)completion {
    if (count == 0) {
        completion(nil);
        return;
    }
    
    [[M2XController sharedInstance] updateStream:streamModel->stream
                                    withNewValue:[[streamModel value] intValue] + count
                            andCompletionHandler:^(NSError *error) {
        if (error) {
            [SVProgressHUD showErrorWithStatus:error.localizedDescription];
            completion(error);
        } else {
            [[M2XController sharedInstance] populateModelsWithCompletionHandler:^(NSError *error) {
                completion(error);
            }];
        }
    }];
}

- (IBAction)confirmTapped:(id)sender {
    [UIView animateWithDuration:0.2 animations:^{
        [self.confirmView setAlpha:0.0];
        [self.loopView setAlpha:1.0];
    }];
    
    if ([[self.typeParam lowercaseString] containsString:kM2XGlassType]) {
        self.binType = kM2XGlassType;
    } else if ([[self.typeParam lowercaseString] containsString:kM2XPlasticType]) {
        self.binType = kM2XPlasticType;
    } else if ([[self.typeParam lowercaseString] containsString:kM2XPaper]) {
        self.binType = kM2XPaper;
    } else {
        self.binType = kM2XMetal;
    }
    
    [[BluetoothManager sharedInstance] setDelegate:self];
    [[BluetoothManager sharedInstance] openBinType:self.binType];
}

- (void)dropItem {
    for (UIView *subview in self.containerView.subviews) {
        [subview removeFromSuperview];
    }
    [self.animator removeAllBehaviors];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:self.binType]];
    [imageView setCenter:CGPointMake(CGRectGetMidX(self.containerView.bounds),
                                     -CGRectGetMidY(self.containerView.bounds) * 0.5)];
    [imageView setTransform:CGAffineTransformMakeRotation(M_PI / -8.0)];
    [self.containerView addSubview:imageView];
    
    UIGravityBehavior *gravityBehavior = [[UIGravityBehavior alloc] initWithItems:@[imageView]];
    [self.animator addBehavior:gravityBehavior];
    
    UICollisionBehavior *collisionBehavior = [[UICollisionBehavior alloc] initWithItems:@[imageView]];
    [collisionBehavior addBoundaryWithIdentifier:@"bottom"
                                       fromPoint:CGPointMake(0.0,
                                                             CGRectGetHeight(self.containerView.bounds) + 50.0)
                                         toPoint:CGPointMake(CGRectGetWidth(self.containerView.bounds),
                                                             CGRectGetHeight(self.containerView.bounds) - 40.0)];
    [self.animator addBehavior:collisionBehavior];

    if ([[self.typeParam lowercaseString] containsString:kM2XGlassType]) {
        self.glassCount++;
    } else if ([[self.typeParam lowercaseString] containsString:kM2XPlasticType]) {
        self.plasticCount++;
    } else if ([[self.typeParam lowercaseString] containsString:kM2XPaper]) {
        self.paperCount++;
    } else {
        self.metalCount++;
    }
}

- (void)bluetoothManagerDepotConnected {
}

- (void)bluetoothManagerDepotDisconnected {
}

- (void)bluetoothManagerItemWasDeposited {
    [self dropItem];
}

- (void)bluetoothManagerDepositTimedOut {
    NSLog(@"bluetoothManagerDepositTimedOut");
}

- (IBAction)dropTest:(id)sender {
    //[self dropItem];
}

@end
