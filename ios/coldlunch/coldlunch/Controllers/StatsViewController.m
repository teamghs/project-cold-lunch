//
//  StatsViewController.m
//  coldlunch
//
//  Created by Jordan Hesse on 2016-01-02.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import "StatsViewController.h"
#import "M2XController.h"
#import "LevelModel.h"
#import "UIView+ProgressRing.h"

#import <SDWebImage/UIImageView+WebCache.h>

@interface StatsViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *graphImageView;
@property (weak, nonatomic) IBOutlet UIView *cityProgressView;
@property (weak, nonatomic) IBOutlet UIView *neighborhoodProgressView;

@end

@implementation StatsViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    NSString *urlString = [NSString stringWithFormat:@"http://api-m2x.att.com/v2/charts/f4b54dd3ce7e3fba4555f77b93545b47.png?width=750&height=350&type=values"];
    [self.graphImageView sd_setImageWithURL:[NSURL URLWithString:urlString]
                           placeholderImage:nil
                                    options:SDWebImageCacheMemoryOnly
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                      if (image) {
                                          [self.graphImageView setImage:image];
                                          
                                          if (cacheType != SDImageCacheTypeMemory) {
                                              CATransition *animation = [CATransition animation];
                                              [animation setDuration:0.2];
                                              [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
                                              [animation setType:kCATransitionFade];
                                              [self.graphImageView.layer addAnimation:animation forKey:@"crossFade"];
                                          }
                                      }
                                  }];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    LevelModel* level = [[LevelModel alloc] init];
    float neighborHoodProgress = (float)[[M2XController sharedInstance] calculateTotalsFromModels] / (float)level.neighborHoodLevelUpMax;
    float cityProgress = (float)[[M2XController sharedInstance] calculateTotalsFromModels] / (float)level.cityLevelUpMax;
    
    [self.cityProgressView animateProgress:cityProgress
                                     color:[UIColor whiteColor]
                                 lineWidth:7.0
                                     inset:9.0
                                  duration:0.8];
    [self.neighborhoodProgressView animateProgress:neighborHoodProgress
                                             color:[UIColor whiteColor]
                                         lineWidth:7.0
                                             inset:9.0
                                          duration:0.8];
}

- (UIImage *)imageWithChromaKeyMasking:(UIImage *)image {
    const CGFloat colorMasking[6] = { 250.0, 255.0, 250.0, 255.0, 250.0, 255.0 };
    CGImageRef oldImage = image.CGImage;
    CGBitmapInfo newInfo = (kCGImageAlphaNoneSkipLast | kCGBitmapByteOrder32Big);
    CGDataProviderRef provider = CGImageGetDataProvider(oldImage);
    CGImageRef noAlphaImageRef = CGImageCreate(image.size.width, image.size.height,
                                               CGImageGetBitsPerComponent(oldImage),
                                               CGImageGetBitsPerPixel(oldImage),
                                               CGImageGetBytesPerRow(oldImage),
                                               CGImageGetColorSpace(oldImage),
                                               newInfo, provider, NULL, false,
                                               kCGRenderingIntentDefault);
    CGDataProviderRelease(provider); provider = NULL;
    CGImageRef newImageRef = CGImageCreateWithMaskingColors(noAlphaImageRef, colorMasking);
    UIImage *imageRef = [UIImage imageWithCGImage:newImageRef];
    CGImageRelease(noAlphaImageRef);
    CGImageRelease(newImageRef);
    return imageRef;
}

@end
