//
//  ProfileViewController.m
//  coldlunch
//
//  Created by Jordan Hesse on 2016-01-02.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import "ProfileViewController.h"
#import "M2XController.h"
#import "UIView+ProgressRing.h"
#import "LevelModel.h"

@interface ProfileViewController ()

@property (strong, nonatomic) UIColor *ringColor;

@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;

@property (weak, nonatomic) IBOutlet UIView *progressView;
@property (weak, nonatomic) IBOutlet UILabel *itemCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;
@property (weak, nonatomic) IBOutlet UILabel *nextItemCountLabel;

@property (weak, nonatomic) IBOutlet UIView *paperProgressView;
@property (weak, nonatomic) IBOutlet UIView *plasticProgressView;
@property (weak, nonatomic) IBOutlet UIView *metalProgressView;
@property (weak, nonatomic) IBOutlet UIView *glassProgressView;

@property (nonatomic) int currentLevel;

@property (strong, nonatomic) LevelModel *levelModel;

@end

@implementation ProfileViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.ringColor = [UIColor colorWithRed:144.0/255.0 green:198.0/255.0 blue:50.0/255.0 alpha:1.0];
    self.levelModel = [[LevelModel alloc] init];
    [self.profileImageView setImage:[UIImage imageNamed:@"profile-picture"]];
    [self.nameLabel setText:@"Chris Waind"];
    [self.locationLabel setText:@"North Vancouver"];
    
    int itemTotal = [[M2XController sharedInstance] calculateTotalsFromModels];
    self.currentLevel = (itemTotal / self.levelModel.levelUpMax) + 1;
    int itemsToNextLevel =  (self.levelModel.levelUpMax - [self getValueForLevel:itemTotal]);
    
    [self.itemCountLabel setText:[NSString stringWithFormat:@"You have collected a total of %zd items", itemTotal]];
    [self.levelLabel setText:[NSString stringWithFormat:@"LEVEL %zd", self.currentLevel]];
    [self.nextItemCountLabel setText:[NSString stringWithFormat:@"%zd items", itemsToNextLevel]];
    
    float levelProgress = ((float)[self getValueForLevel:[[M2XController sharedInstance] calculateTotalsFromModels]] / (float)self.levelModel.levelUpMax);
    [self.progressView animateProgress:levelProgress
                                 color:self.ringColor
                             lineWidth:10.0
                                 inset:17.0
                              duration:1.0];
    
    float paperProgress = [self getModelValueForLevel:[[M2XController sharedInstance] paperModel]] / (float)self.levelModel.itemMax;
    float glassProgress = [self getModelValueForLevel:[[M2XController sharedInstance] glassModel]] / (float)self.levelModel.itemMax;
    float plasticProgress = [self getModelValueForLevel:[[M2XController sharedInstance] plasticModel]] / (float)self.levelModel.itemMax;
    float metalProgress = [self getModelValueForLevel:[[M2XController sharedInstance] metalModel]] / (float)self.levelModel.itemMax;
    
    [self animateSmallRingInView:self.paperProgressView progress:paperProgress];
    [self animateSmallRingInView:self.plasticProgressView progress:plasticProgress];
    [self animateSmallRingInView:self.metalProgressView progress:metalProgress];
    [self animateSmallRingInView:self.glassProgressView progress:glassProgress];
}

-(float)getModelValueForLevel:(M2XStreamModel *)model {
    return (float)[[M2XController sharedInstance] getNormalizedValueFromTotal:[model.value intValue] againstMaximum:self.levelModel.itemMax atLevel:self.currentLevel];
}

-(float)getValueForLevel:(int)value {
    return (float)[[M2XController sharedInstance] getNormalizedValueFromTotal:value againstMaximum:self.levelModel.levelUpMax atLevel:self.currentLevel];
}

- (void)animateSmallRingInView:(UIView *)view progress:(CGFloat)progress {
    [view animateProgress:progress color:self.ringColor lineWidth:4.0 inset:6.0 duration:0.7];
}

@end
