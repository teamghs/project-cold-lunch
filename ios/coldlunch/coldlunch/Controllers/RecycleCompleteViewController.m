//
//  RecycleCompleteViewController.m
//  coldlunch
//
//  Created by Jordan Hesse on 2016-01-02.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import "RecycleCompleteViewController.h"
#import "DataRewardController.h"
#import "M2XController.h"
#import "SMSManager.h"
#import "MouthManager.h"
#import "SpeechManager.h"
#import "LevelModel.h"
#import <Toast/UIView+Toast.h>

@import AVFoundation;

@interface RecycleCompleteViewController () <AVAudioPlayerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *paperCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *metalCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *plasticCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *glassCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *dataCountLabel;

@property (strong, nonatomic) AVAudioPlayer *audioPlayer;

@end

@implementation RecycleCompleteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [_paperCountLabel setText:[NSString stringWithFormat:@"%i", _paperCount]];
    [_plasticCountLabel setText:[NSString stringWithFormat:@"%i", _plasticCount]];
    [_metalCountLabel setText:[NSString stringWithFormat:@"%i", _metalCount]];
    [_glassCountLabel setText:[NSString stringWithFormat:@"%i", _glassCount]];
    
    [self.dataCountLabel setText:@"20"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    [self.audioPlayer setDelegate:nil];
    self.audioPlayer = nil;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self playCompletedMessage];

    if ([DataRewardController sharedInstance].userHasOptedIn) {
        [[DataRewardController sharedInstance] rewardDataWithResult:^(NSError *error) {
            if (error) {
                NSLog(@"%@", error);
            } else {
                [self.view makeToast:@"Data awarded!"];
            }
        }];
    }
    
    if ([self shouldNotify]) {
        [[SMSManager sharedInstance] sendMessageWithCompletion:^(id responseData, NSError *error) {
            NSLog(@"SMS Sent");
        }];
    }
}

-(BOOL)shouldNotify {
    LevelModel *model = [[LevelModel alloc] init];
    float neighborHoodProgress =  model.neighborHoodLevelUpMax - [[M2XController sharedInstance] calculateTotalsFromModels];
    return neighborHoodProgress < 3;
}

- (void)playCompletedMessage {
    NSError *error = nil;
    
    [[AVAudioSession sharedInstance] setActive:YES error:&error];
    if (error) {
        return;
    }
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    if (error) {
        return;
    }
    
    int total = _glassCount + _plasticCount + _paperCount + _metalCount;
    NSString *message = [NSString stringWithFormat:@"Nice work, Chris! That's %zd %@ recycled",
                         total, (total == 1 ? @"item" : @"items")];
    [[SpeechManager sharedInstance] textToSpeech:message
                                  withCompletion:^(id responseData, NSError *responseError) {
                                      if (responseError) {
                                          return;
                                      }
                                      
                                      NSError *error = nil;
                                      [[AVAudioSession sharedInstance] setActive:YES error:&error];
                                      if (error) {
                                          return;
                                      }
                                      
                                      self.audioPlayer = [[AVAudioPlayer alloc] initWithData:responseData error:&error];
                                      if ([self.audioPlayer prepareToPlay])
                                      {
                                          if (error) {
                                              [self performSegueWithIdentifier:@"Voice" sender:nil];
                                              return;
                                          }
                                          
                                          [self.audioPlayer setDelegate:self];
                                          [self.audioPlayer play];
                                          
                                          [[MouthManager sharedInstance] startTalking];
                                      } else {
                                          [[MouthManager sharedInstance] stopTalking];
                                          
                                          [self performSegueWithIdentifier:@"Voice" sender:nil];
                                      }
                                  }];
    
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    [[MouthManager sharedInstance] stopTalking];
}

@end
