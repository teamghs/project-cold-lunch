//
//  RecycleCompleteViewController.h
//  coldlunch
//
//  Created by Jordan Hesse on 2016-01-02.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecycleCompleteViewController : UIViewController
@property (nonatomic) int plasticCount;
@property (nonatomic) int glassCount;
@property (nonatomic) int paperCount;
@property (nonatomic) int metalCount;
@end
