//
//  IntroViewController.m
//  coldlunch
//
//  Created by Jordan Hesse on 2016-01-02.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import "IntroViewController.h"

#import "M2XController.h"
#import "SpeechManager.h"
#import "SMSManager.h"

#import <SVProgressHUD/SVProgressHUD.h>

@interface IntroViewController ()

@end

@implementation IntroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    __block NSUInteger count = 2;
    void (^internalCompletion)(void) = ^{
        --count;
        if (count > 0) {
            return;
        }
        
        [self performSegueWithIdentifier:@"Dashboard" sender:nil];
    };
    
    [[SpeechManager sharedInstance] loginWithCompletion:^(id responseData, NSError *error) {
        if (error) {
            [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        } else {
            internalCompletion();
        }
    }];
    
    [[M2XController sharedInstance] populateModelsWithCompletionHandler:^(NSError *error) {
        if (error) {
            [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        } else {
            [self resetValuesForDemoWithCompletionHandler:^(NSError *error) {
                [[M2XController sharedInstance] populateModelsWithCompletionHandler:^(NSError *error) {
                    internalCompletion();
                }];
            }];
        }
    }];
    
}

- (void)resetValuesForDemoWithCompletionHandler:(ModelsRetrievedCompletionBlock)completion {
    M2XStream* plastic = [[M2XController sharedInstance] plasticModel]->stream;
    M2XStream* paper = [[M2XController sharedInstance] paperModel]->stream;
    M2XStream* metal = [[M2XController sharedInstance] metalModel]->stream;
    M2XStream* glass = [[M2XController sharedInstance] glassModel]->stream;
    
    [plastic updateValue:[NSNumber numberWithInt:375] timestamp:[[NSDate date] toISO8601] completionHandler:^(M2XResponse *response) {
        [paper updateValue:[NSNumber numberWithInt:375] timestamp:[[NSDate date] toISO8601] completionHandler:^(M2XResponse *response) {
            [metal updateValue:[NSNumber numberWithInt:400] timestamp:[[NSDate date] toISO8601] completionHandler:^(M2XResponse *response) {
                [glass updateValue:[NSNumber numberWithInt:447] timestamp:[[NSDate date] toISO8601] completionHandler:^(M2XResponse *response) {
                    completion(nil);
                }];
            }];
        }];
    }];
}

@end
