//
//  DepotViewController.m
//  coldlunch
//
//  Created by Jordan Hesse on 2016-01-02.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import "DepotViewController.h"

@import MapKit;
@import CoreLocation;

@interface DepotViewController () <MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (assign, nonatomic) BOOL zoomed;

@end

@interface DepotAnnotation : NSObject <MKAnnotation>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;

@end

@implementation DepotViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.locationManager = [[CLLocationManager alloc] init];
    [self.locationManager requestWhenInUseAuthorization];
    
    [self.mapView.layer setCornerRadius:11.0];
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    [self zoomOnCoordinate:mapView.userLocation.coordinate radius:1000.0 animated:YES];
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    if (self.zoomed && [mapView.annotations count] == 1) {
        MKMapPoint location = MKMapPointForCoordinate(mapView.userLocation.coordinate);
        location.x += MKMapPointsPerMeterAtLatitude(mapView.userLocation.coordinate.latitude) * 250.0;
        location.y += MKMapPointsPerMeterAtLatitude(mapView.userLocation.coordinate.longitude) * 350.0;
        
        DepotAnnotation *annotation = [[DepotAnnotation alloc] init];
        [annotation setCoordinate:MKCoordinateForMapPoint(location)];
        [mapView addAnnotation:annotation];
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[DepotAnnotation class]]) {
        MKPinAnnotationView *pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation
                                                                       reuseIdentifier:@"Depot"];
        [pinView setAnimatesDrop:YES];
        [pinView setCanShowCallout:YES];
        return pinView;
    } else {
        return nil;
    }
}

- (void)zoomOnCoordinate:(CLLocationCoordinate2D)coordinate radius:(CGFloat)radius animated:(BOOL)animated {
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(coordinate, radius, radius);
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:viewRegion];
    [self.mapView setRegion:adjustedRegion animated:animated];
    self.zoomed = YES;
}

@end

@implementation DepotAnnotation

@synthesize coordinate = _coordinate;

- (NSString *)title {
    return @"SmartWaste Depot #787";
}

@end