//
//  RecycleVoiceViewController.m
//  coldlunch
//
//  Created by Jordan Hesse on 2016-01-02.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import "RecycleVoiceViewController.h"
#import "RecycleConfirmViewController.h"

#import "SpeechManager.h"
#import "ATTSpeechResultModel.h"

#import <Toast/UIView+Toast.h>
#import <EZAudio/EZAudio.h>

@import AVFoundation;

@interface RecycleVoiceViewController () <AVAudioRecorderDelegate, EZMicrophoneDelegate, EZRecorderDelegate>

@property (weak, nonatomic) IBOutlet EZAudioPlotGL *audioPlot;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityView;

@property (strong, nonatomic) AVAudioRecorder *audioRecorder;
@property (strong, nonatomic) EZMicrophone *microphone;

@end

@implementation RecycleVoiceViewController

- (void)dealloc
{
    self.audioRecorder.delegate = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.audioPlot setBackgroundColor:[UIColor clearColor]];
    [self.audioPlot setColor:[UIColor colorWithRed:137.0/255.0 green:78.0/255.0 blue:235.0/255.0 alpha:1.0]];
    [self.audioPlot setPlotType:EZPlotTypeRolling];
    [self.audioPlot setShouldFill:YES];
    [self.audioPlot setShouldMirror:YES];
    
    [self.activityView stopAnimating];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self startRecording];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self stopRecording:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController isKindOfClass:[RecycleConfirmViewController class]]) {
        NSString *typeParam = [NSString stringWithFormat:@"“%@”", sender];
        [(RecycleConfirmViewController *)segue.destinationViewController setTypeParam:typeParam];
    }
}

- (IBAction)unwindToVoice:(UIStoryboardSegue *)segue {
}

- (void)startRecording {
    NSError *error = nil;
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryRecord error:&error];
    if (error) {
        return;
    }
    [[AVAudioSession sharedInstance] setActive:YES error:&error];
    if (error) {
        return;
    }
    
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:@[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                                                              @"recording.wav"
                                                              ]];
    
    NSDictionary *settings = @{ AVFormatIDKey : @(kAudioFormatLinearPCM),
                                AVSampleRateKey : @(8000.0),
                                AVNumberOfChannelsKey : @(1),
                                AVLinearPCMBitDepthKey : @(16),
                                AVLinearPCMIsBigEndianKey : @(NO),
                                AVLinearPCMIsFloatKey : @(NO),
                                AVLinearPCMIsNonInterleaved : @(NO),
                                AVEncoderAudioQualityKey : @(AVAudioQualityMax),
                                };
    
    // Clean up left overs
    [self.audioRecorder setDelegate:nil];
    
    self.audioRecorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:settings error:&error];
    if (error) {
        return;
    }
    
    [self.audioRecorder setDelegate:self];
    [self.audioRecorder setMeteringEnabled:YES];
    [self.audioRecorder prepareToRecord];
    [self.audioRecorder record];

    self.microphone = [EZMicrophone microphoneWithDelegate:self];
    [self.microphone startFetchingAudio];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self stopRecording:NO];
    });
}

- (void)stopRecording:(BOOL)cancelled {
    [self.microphone stopFetchingAudio];
    
    if (cancelled) {
        [self.audioRecorder setDelegate:nil];
    }
    [self.audioRecorder stop];
}

- (void)recorderDidClose:(EZRecorder *)recorder {
    // do stuff
}

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag {
    NSData *speechData = [NSData dataWithContentsOfURL:self.audioRecorder.url];
    if (!speechData) {
        [self startRecording];
        return;
    }

    [self.activityView startAnimating];
    [[SpeechManager sharedInstance] convertSpeech:speechData
                                             type:@"audio/raw;coding=linear;rate=8000;byteorder=LE"
                                   withCompletion:^(id responseData, NSError *error) {
                                       [self.activityView stopAnimating];
                                       if (error) {
                                           [self.view makeToast:error.localizedDescription];
                                           [self startRecording];
                                       } else {
                                           NSString *result = [[[[responseData Recognition] NBest] firstObject] ResultText];
                                           NSString *parsedResult = [self checkKeywordInResult:result];
                                           if (parsedResult) {
                                               [self performSegueWithIdentifier:@"Confirm" sender:parsedResult];
                                           } else {
                                               [self.view makeToast:@"Sorry, I didn't get that..."];
                                               [self startRecording];
                                           }
                                       }
                                   }];
}

- (NSString *)checkKeywordInResult:(NSString *)result {
    if ([result containsString:@"plastic"]) {
        return @"Plastic Bottle";
    }
    if ([result containsString:@"paper"]) {
        return @"Paper";
    }
    if ([result containsString:@"metal"]) {
        return @"Metal";
    }
    if ([result containsString:@"glass"]) {
        return @"Glass Bottle";
    }
    if ([result containsString:@"soda can"]) {
        return @"Soda Can";
    }
    if ([result containsString:@"tin can"]) {
        return @"Tin Can";
    }
    return nil;
}

- (void)microphone:(EZMicrophone *)microphone hasAudioReceived:(float **)buffer
    withBufferSize:(UInt32)bufferSize withNumberOfChannels:(UInt32)numberOfChannels {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.audioPlot updateBuffer:buffer[0] withBufferSize:bufferSize];
    });
}

@end
