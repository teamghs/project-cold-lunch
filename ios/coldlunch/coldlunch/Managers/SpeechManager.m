//
//  SpeechManager.m
//  coldlunch
//
//  Created by Jordan Hesse on 2016-01-02.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import "SpeechManager.h"
#import "AFNetworkActivityLogger.h"

#import "ATTOAuthModel.h"
#import "ATTSpeechResultModel.h"

#import <AFNetworking/AFHTTPSessionManager.h>

@interface SpeechManager ()

@property (strong, nonatomic) AFHTTPSessionManager *requestManager;
@property (strong, nonatomic) ATTOAuthModel *oauthModel;
@property (strong, nonatomic) AVAudioPlayer *player;

@end

@implementation SpeechManager

+ (instancetype)sharedInstance {
    static SpeechManager* s_Instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s_Instance = [[SpeechManager alloc] init];
    });
    return s_Instance;
}

- (id)init {
    if (self = [super init]) {
        _requestManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://api.att.com/"]];
        [[AFNetworkActivityLogger sharedLogger] startLogging];
        
        // Response serializer
        AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializer];
        [responseSerializer setReadingOptions:NSJSONReadingAllowFragments];
        [self.requestManager setResponseSerializer:responseSerializer];
    }
    return self;
}

- (void)loginWithCompletion:(SpeechRequestCompletionBlock)completion {
    [self.requestManager POST:@"oauth/v4/token"
                   parameters:@{@"client_id" : @"n4smx27yo9lymskj4xoz5fyvtbe3iic9",
                                @"client_secret" : @"asph3ptksf6p0m3fq8mfrbymcmkunx4o",
                                @"grant_type" : @"client_credentials",
                                @"scope" : @"SPEECH,TTS",
                                }
                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
                          NSError *parseError = nil;
                          self.oauthModel = [[ATTOAuthModel alloc] initWithDictionary:responseObject error:&parseError];
                          if (parseError) {
                              completion(nil, parseError);
                          } else {
                              // Request serializer
                              AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
                              [requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", self.oauthModel.accessToken]
                                       forHTTPHeaderField:@"Authorization"];
                              [self.requestManager setRequestSerializer:requestSerializer];

                              completion(self.oauthModel, nil);
                          }
                      } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                          completion(nil, error);
                      }];
}

-(void)textToSpeech:(NSString *)textToSpeak withCompletion:(SpeechRequestCompletionBlock)completion {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSURL *url = [NSURL URLWithString:@"https://api.att.com/speech/v3/textToSpeech"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    [request addValue:@"text/plain" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[NSString stringWithFormat:@"Bearer %@", self.oauthModel.accessToken] forHTTPHeaderField:@"Authorization"];
//po    [request addValue:[NSString stringWithFormat:@"%lu",(unsigned long)[textToSpeak length]] forHTTPHeaderField:@"Content-length"];
    [request addValue:@"audio/x-wav" forHTTPHeaderField:@"Accept"];
    [request addValue:@"VoiceName=mike" forHTTPHeaderField:@"X-Arg"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[textToSpeak dataUsingEncoding:NSASCIIStringEncoding]];

    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([[NSThread currentThread] isMainThread]) {
                completion(data, nil);
            } else {
                NSLog(@"Not in main thread--completion handler");
            }
        });
    }];
    [postDataTask resume];
}

- (void)convertSpeech:(NSData *)speechData type:(NSString *)contentType withCompletion:(SpeechRequestCompletionBlock)completion {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSURL *url = [NSURL URLWithString:@"https://api.att.com/speech/v3/speechToText"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    [request addValue:[NSString stringWithFormat:@"Bearer %@", self.oauthModel.accessToken] forHTTPHeaderField:@"Authorization"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue:[NSString stringWithFormat:@"%zd", speechData.length] forHTTPHeaderField:@"Content-length"];
    [request addValue:@"keep-alive" forHTTPHeaderField:@"Connection"];
    [request addValue:@"BusinessSearch" forHTTPHeaderField:@"X-SpeechContext"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:speechData];
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary * json  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        ATTSpeechResultModel *result = [[ATTSpeechResultModel alloc] initWithDictionary:json error:&error];
        dispatch_async(dispatch_get_main_queue(), ^{

            if ([[NSThread currentThread] isMainThread]){
                if (error) {
                    completion(nil, error);
                } else {
                    completion(result, nil);
                }
            }
            else{
                NSLog(@"Not in main thread--completion handler");
            }
        });
    }];
    [postDataTask resume];
}

@end
