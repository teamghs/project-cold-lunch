//
//  MouthManager.h
//  coldlunch
//
//  Created by Jordan Hesse on 2016-01-03.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MouthManager : NSObject

+ (instancetype)sharedInstance;

- (void)connect;

- (void)startTalking;
- (void)stopTalking;

@end
