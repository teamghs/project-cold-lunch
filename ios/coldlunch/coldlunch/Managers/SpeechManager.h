//
//  SpeechManager.h
//  coldlunch
//
//  Created by Jordan Hesse on 2016-01-02.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

typedef void(^SpeechRequestCompletionBlock) (id responseData, NSError *error);

@interface SpeechManager : NSObject <AVAudioPlayerDelegate>

+ (instancetype)sharedInstance;

- (void)loginWithCompletion:(SpeechRequestCompletionBlock)completion;

- (void)convertSpeech:(NSData *)speechData
                 type:(NSString *)contentType
       withCompletion:(SpeechRequestCompletionBlock)completion;

-(void)textToSpeech:(NSString *)textToSpeak
     withCompletion:(SpeechRequestCompletionBlock)completion;

@end
