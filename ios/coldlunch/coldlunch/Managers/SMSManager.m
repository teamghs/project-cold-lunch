//
//  SMSManager.m
//  coldlunch
//
//  Created by Evan Lewis on 1/2/16.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import "SMSManager.h"
#import "AFNetworkActivityLogger.h"
#import "ATTOAuthModel.h"

#import <AFNetworking/AFHTTPSessionManager.h>

@interface SMSManager ()

@property (strong, nonatomic) AFHTTPSessionManager *requestManager;
@property (strong, nonatomic) ATTOAuthModel *oauthModel;

@end

@implementation SMSManager

+ (instancetype)sharedInstance {
    static SMSManager* s_Instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s_Instance = [[SMSManager alloc] init];
    });
    return s_Instance;
    
}

- (instancetype)init
{
    if (self = [super init]) {
        _requestManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://api.att.com/"]];
        [[AFNetworkActivityLogger sharedLogger] startLogging];
        
        AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializer];
        [responseSerializer setReadingOptions:NSJSONReadingAllowFragments];
        [self.requestManager setResponseSerializer:responseSerializer];
        
        AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
        [self.requestManager setRequestSerializer:requestSerializer];
    }
    return self;
}

-(void)loginWithCompletion:(SMSRequestCompletionBlock)completion {
    [self.requestManager POST:@"oauth/v4/token" parameters:@{@"client_id" : @"kpbipnfewxf94tnfd93mryx4kuhdlheq", @"client_secret" : @"wwi37m7xnt8rmexlxixg7lsh3panydhn", @"grant_type" : @"authorization_code", @"code" : @"BF-ACSI~3~20160102220859~2Q6AtCMvyPKyg1krveSY", @"scope" : @"IMMN"
        }
                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
                          NSError* parseError = nil;
                          self.oauthModel = [[ATTOAuthModel alloc] initWithDictionary:responseObject error:&parseError];
                          if (parseError) {
                              completion(nil, parseError);
                          } else {
                              completion(self.oauthModel, nil);
                          }
                          
                      } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                          completion(nil, error);
                      }];
}

-(void)sendMessageWithCompletion:(SMSRequestCompletionBlock)completion {
    [self.requestManager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", @"BF-ACSI~3~20160103182834~UxwVxo53Ehi6gUgoK9gYSZ4WxpffthbB"]
                                 forHTTPHeaderField:@"Authorization"];
    [self.requestManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"content-type"];
    
    
    [self.requestManager POST:@"myMessages/v2/messages" parameters:@{ @"messageRequest" : @{@"addresses" : @[@"tel:3036193484", @"tel:3037269931"], @"text" : @"Your neightborhood is about to hit their recycling goal!"} } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *data = error.userInfo[@"com.alamofire.serialization.response.error.data"];
        NSString *whoops = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"%@", whoops);
    }];
}


@end
