//
//  DataRewardController.h
//  coldlunch
//
//  Created by Daryl at Finger Foods on 2016-01-02.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

typedef void(^DataRewardCallback)(NSError *error);

@interface DataRewardController : NSObject
+ (instancetype)sharedInstance;
- (void)showOptInFromViewController:(UIViewController *)viewController;
- (void)rewardDataWithResult:(DataRewardCallback)callback;

@property (readonly, nonatomic) BOOL userHasOptedIn;
@end
