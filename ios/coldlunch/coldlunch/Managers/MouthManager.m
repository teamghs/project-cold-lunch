//
//  MouthManager.m
//  coldlunch
//
//  Created by Jordan Hesse on 2016-01-03.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import "MouthManager.h"

#import <Pulse2SDK/HMNPulse2API.h>

@interface MouthManager ()

@property (assign, nonatomic) BOOL isTalking;

@end

@implementation MouthManager

+ (instancetype)sharedInstance {
    static MouthManager* s_Instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s_Instance = [[MouthManager alloc] init];
    });
    return s_Instance;
}

- (id)init {
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(deviceConnected:)
                                                     name:EVENT_DEVICE_CONNECTED
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(deviceDisconnected:)
                                                     name:EVENT_DEVICE_DISCONNECTED
                                                   object:nil];
    }
    return self;
}

- (void)deviceConnected:(NSNotification *)notification {
    [self stopTalking];
}

- (void)deviceDisconnected:(NSNotification *)notification {
    [self connect];
}

- (void)connect {
    [HMNDeviceGeneral connectToMasterDevice];
}

- (void)startTalking {
    [HMNLedControl setLedPattern:HMNPattern_Rain withData:nil];
    [HMNLedControl setLedBrightness:255.0];
    
    [self performSelector:@selector(stopTalking) withObject:nil afterDelay:5.0];
}

- (void)stopTalking {
    self.isTalking = NO;
    
    [HMNLedControl setLedPattern:HMNPattern_Star withData:nil];
    [HMNLedControl setLedBrightness:25.0];
}

//- (void)frown {
//    UIColor *onColor = [UIColor colorWithRed:1.0 green:0.0 blue:1.0 alpha:1.0];
//    UIColor *offColor = [UIColor colorWithRed:0.0 green:1.0 blue:0.0 alpha:1.0];
//
////    [HMNLedControl setLedPattern:-1 withData:@[@(1), @(1), @(1), @(1), @(1), @(1), @(1), @(1), @(1), @(1), @(1),
////                                                              @(1), @(1), @(1), @(1), @(1), @(1), @(1), @(1), @(1), @(1), @(1),
////                                                              @(1), @(1), @(1), @(1), @(1), @(1), @(1), @(1), @(1), @(1), @(1),
////                                                              @(1), @(1), @(1), @(0), @(0), @(0), @(0), @(0), @(1), @(1), @(1),
////                                                              @(1), @(1), @(1), @(0), @(0), @(0), @(0), @(0), @(1), @(1), @(1),
////                                                              @(1), @(1), @(1), @(0), @(0), @(0), @(0), @(0), @(1), @(1), @(1),
////                                                              @(1), @(1), @(1), @(1), @(1), @(1), @(1), @(1), @(1), @(1), @(1),
////                                                              @(1), @(1), @(1), @(1), @(1), @(1), @(1), @(1), @(1), @(1), @(1),
////                                                              @(1), @(1), @(1), @(1), @(1), @(1), @(1), @(1), @(1), @(1), @(1),
////                                                              ]];
//    
//    [HMNLedControl setLedPattern:HMNPattern_Rain withData:nil];
//
//    [HMNLedControl setBackgroundColor:onColor propagateToSlaveDevice:NO];
//
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [HMNLedControl setLedBrightness:0.0];
//    });
//
////    [HMNLedControl setColorImage:@[offColor, offColor, offColor, offColor, offColor, offColor, offColor, offColor, offColor, offColor, offColor,
////                                   offColor, offColor, offColor, offColor, offColor, offColor, offColor, offColor, offColor, offColor, offColor,
////                                   offColor, offColor, offColor, offColor, offColor, offColor, offColor, offColor, offColor, offColor, offColor,
////                                   offColor, offColor, offColor, onColor,  onColor,  onColor,  onColor,  onColor,  offColor, offColor, offColor,
////                                   offColor, offColor, offColor, onColor,  onColor,  onColor,  onColor,  onColor,  offColor, offColor, offColor,
////                                   offColor, offColor, offColor, onColor,  onColor,  onColor,  onColor,  onColor,  offColor, offColor, offColor,
////                                   offColor, offColor, offColor, offColor, offColor, offColor, offColor, offColor, offColor, offColor, offColor,
////                                   offColor, offColor, offColor, offColor, offColor, offColor, offColor, offColor, offColor, offColor, offColor,
////                                   offColor, offColor, offColor, offColor, offColor, offColor, offColor, offColor, offColor, offColor, offColor,
////                                   ]];
//}
//
//- (void)smile {
//    [HMNLedControl setBackgroundColor:[UIColor yellowColor] propagateToSlaveDevice:NO];
//}

@end
