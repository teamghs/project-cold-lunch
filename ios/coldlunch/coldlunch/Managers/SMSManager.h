//
//  SMSManager.h
//  coldlunch
//
//  Created by Evan Lewis on 1/2/16.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SMSRequestCompletionBlock) (id responseData, NSError *error);

@interface SMSManager : NSObject


+ (instancetype)sharedInstance;

- (void)loginWithCompletion:(SMSRequestCompletionBlock)completion;
- (void)sendMessageWithCompletion:(SMSRequestCompletionBlock)completion;

@end
