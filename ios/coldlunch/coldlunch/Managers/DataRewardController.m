//
//  DataRewardController.m
//  coldlunch
//
//  Created by Daryl at Finger Foods on 2016-01-02.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import "DataRewardController.h"
#import <Toast/UIView+Toast.h>
@import MessageUI;
@import AdSupport;
#import <AFNetworking/AFNetworking.h>

static NSString *kOptInKey = @"DataRewardOptInKey";

@interface DataRewardController () <MFMessageComposeViewControllerDelegate>
@property (weak, nonatomic) UIViewController *presentingViewController;
@property (assign, nonatomic) BOOL hasBeenShownThisSession;
@end

@implementation DataRewardController
+ (instancetype)sharedInstance
{
    static DataRewardController *s_instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s_instance = [[DataRewardController alloc] init];
    });
    
    return s_instance;
}

- (BOOL)userHasOptedIn
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:kOptInKey];
}

- (void)showOptInFromViewController:(UIViewController *)viewController
{
    if (!self.userHasOptedIn && !self.hasBeenShownThisSession) {
        self.presentingViewController = viewController;
        MFMessageComposeViewController *controller =
        [[MFMessageComposeViewController alloc] init];
        if([MFMessageComposeViewController canSendText])
        {
            NSArray* recipients = [NSArray arrayWithObjects:@"+17023816992", nil];
            NSString *adId = [[[ASIdentifierManager sharedManager]
                               advertisingIdentifier] UUIDString];
            controller.body = adId;
            controller.recipients = recipients;
            controller.messageComposeDelegate = self;
            [viewController presentViewController:controller animated:YES completion:nil];
        }
        
        self.hasBeenShownThisSession = YES;
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController
                                      *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    if (result == MessageComposeResultCancelled) {
        [self.presentingViewController.view makeToast:@"SMS send cancelled."];
    } else if (result == MessageComposeResultSent) {
        [self.presentingViewController.view makeToast:@"You opted in to AT&T Data Rewards!"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kOptInKey];
    } else {
        [self.presentingViewController.view makeToast:@"Failed to send SMS."];
    }
}

- (void)rewardDataWithResult:(DataRewardCallback)callback
{
    NSString *urlFormat = @"http://aus-api.cloudmi.datami.com/dev/goapi/action/registration/com.teamghs.coldlunch/%@";
    NSString *url = [NSString stringWithFormat:urlFormat, [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString]];
    [[AFHTTPRequestOperationManager manager] GET:url parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        if (callback) {
            callback(nil);
        }
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        if (callback) {
            callback(error);
        }
    }];
}

@end
