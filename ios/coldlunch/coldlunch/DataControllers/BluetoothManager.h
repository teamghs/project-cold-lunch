//
//  BluetoothManager.h
//  coldlunch
//
//  Created by Daryl at Finger Foods on 2016-01-02.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreBluetooth;

@protocol BluetoothDelegate <NSObject>
- (void)bluetoothManagerDepotConnected;
- (void)bluetoothManagerDepotDisconnected;
- (void)bluetoothManagerItemWasDeposited;
- (void)bluetoothManagerDepositTimedOut;
@end

@interface BluetoothManager : NSObject

@property (weak, nonatomic) id<BluetoothDelegate> delegate;

+ (instancetype)sharedInstance;
- (void)startAdvertising;
- (void)stopAdvertising;
- (void)closeDumpster;
- (void)disconnectFromDumpster;

- (void)openBinType:(NSString *)binType;
@end
