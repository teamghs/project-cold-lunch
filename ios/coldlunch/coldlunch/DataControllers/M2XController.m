//
//  M2XController.m
//  coldlunch
//
//  Created by Daryl at Finger Foods on 2016-01-02.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import "M2XController.h"
#import "M2X.h"
#import "M2XStreamModel.h"

@interface M2XController ()
@property (strong, nonatomic) M2XClient *client;
@end

@implementation M2XController
+ (instancetype)sharedInstance
{
    static M2XController *s_instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s_instance = [[M2XController alloc] initWithApiKey:@"88833a7d48cde3e63ce2bf98dad82cee"];
    });
    
    return s_instance;
}

- (instancetype)initWithApiKey:(NSString *)apiKey
{
    if (self = [super init])
    {
        self.client = [[M2XClient alloc] initWithApiKey:apiKey];
    }
    
    return self;
}

-(void)populateModelsWithCompletionHandler:(ModelsRetrievedCompletionBlock)completion {
    [self.client deviceWithId:kM2XLasVegasDepotId completionHandler:^(M2XDevice *device, M2XResponse *response) {
        [device streamsWithCompletionHandler:^(NSArray *objects, M2XResponse *response) {
            for (M2XStream *stream in objects)
            {
                NSError *error;
                M2XStreamModel *model =  [[M2XStreamModel alloc] initWithDictionary:[stream attributes] error:&error];
                model->stream = stream;

                if (error) {
                    if (completion) {
                        completion(error);
                    }
                    return;
                } else {
                    if ([model.name isEqualToString:kM2XGlassType]) {
                        _glassModel = model;
                    }
                    if ([model.name isEqualToString:kM2XPaper]) {
                        _paperModel = model;
                    }
                    if ([model.name isEqualToString:kM2XPlasticType]) {
                        _plasticModel = model;
                    }
                    if ([model.name isEqualToString:kM2XMetal]) {
                        _metalModel = model;
                    }
                }
            }
            if (completion) {
                completion(nil);
            }
        }];
    }];
}

-(void)updateStream:(M2XStream *)stream withNewValue:(float)value andCompletionHandler:(ValueUpdatedCompletionBlock)completion {
    [stream updateValue:[NSNumber numberWithFloat:value] timestamp:[[NSDate date] toISO8601] completionHandler:^(M2XResponse *response) {
        completion(response.errorObject);
    }];
}


-(int)calculateTotalsFromModels {
    int total = [_glassModel.value intValue] + [_paperModel.value intValue] + [_metalModel.value intValue] + [_plasticModel.value intValue];
    return total;
}

-(int)getNormalizedValueFromTotal:(int) total againstMaximum:(int)maximum atLevel:(int) level{
    return total - (maximum * (level - 1));
}




@end
