//
//  BluetoothManager.m
//  coldlunch
//
//  Created by Daryl at Finger Foods on 2016-01-02.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import "BluetoothManager.h"
@import CoreBluetooth;


#define TRANSFER_SERVICE_UUID_0         @"E20A39F4-73F5-4BC4-A12F-17D1AD07A960"
#define TRANSFER_CHARACTERISTIC_UUID    @"08590F7E-DB05-467E-8757-72F6FAEB13D4"

@interface BluetoothManager () <CBPeripheralManagerDelegate>
@property (strong, nonatomic) CBPeripheralManager *peripheralManager;
@property (strong, nonatomic) CBMutableCharacteristic *characteristic;
@property (assign, nonatomic) BOOL isInited;
@property (strong, nonatomic) NSMutableArray<CBCentral *> *centrals;
@property (strong, nonatomic) NSMutableArray<CBCentral *> *readyCentrals;

@end

@implementation BluetoothManager
+ (instancetype)sharedInstance
{
    static BluetoothManager *s_instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s_instance = [[BluetoothManager alloc] init];
    });
    
    return s_instance;
}

- (instancetype)init
{
    if (self = [super init]) {
        self.peripheralManager = [[CBPeripheralManager alloc] initWithDelegate:self queue:nil];
        self.centrals = [[NSMutableArray alloc] init];
        self.readyCentrals = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void)startAdvertising
{
    [self.peripheralManager startAdvertising:@{
                                               CBAdvertisementDataServiceUUIDsKey:@[[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID_0]],
                                               CBAdvertisementDataLocalNameKey:@"ProjectColdLunch"
                                               }];
}

- (void)stopAdvertising
{
    [self.peripheralManager stopAdvertising];
}

- (void)disconnectFromDumpster
{
    if ([self.readyCentrals count] > 0)
    {
        [self.peripheralManager updateValue:[NSData dataWithBytes:disconnectCommand length:strlen(disconnectCommand)] forCharacteristic:self.characteristic onSubscribedCentrals:self.readyCentrals];
    }
}

- (void)closeDumpster
{
    if ([self.readyCentrals count] > 0)
    {
        [self.peripheralManager updateValue:[NSData dataWithBytes:closeCommand length:strlen(closeCommand)] forCharacteristic:self.characteristic onSubscribedCentrals:self.readyCentrals];
    }
}

- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral
{
    if (peripheral.state == CBPeripheralManagerStatePoweredOn && !self.isInited) {
        CBMutableService *service = [[CBMutableService alloc] initWithType:[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID_0]
                                                                   primary:YES];
        
        CBMutableCharacteristic *characteristic = [[CBMutableCharacteristic alloc] initWithType:[CBUUID UUIDWithString:TRANSFER_CHARACTERISTIC_UUID] properties:CBCharacteristicPropertyWrite | CBCharacteristicPropertyRead | CBCharacteristicPropertyNotify value:nil permissions:CBAttributePermissionsWriteable | CBAttributePermissionsReadable];
        service.characteristics = @[characteristic];
        
        self.characteristic = characteristic;
        
        [self.peripheralManager addService:service];
        
        self.isInited = YES;
    }
}

- (void)peripheralManager:(CBPeripheralManager *)peripheral didAddService:(CBService *)service error:(NSError *)error
{
}

- (void)peripheralManager:(CBPeripheralManager *)peripheral central:(CBCentral *)central didSubscribeToCharacteristic:(CBCharacteristic *)characteristic
{
    [self.centrals addObject:central];
}

- (void)peripheralManager:(CBPeripheralManager *)peripheral central:(CBCentral *)central didUnsubscribeFromCharacteristic:(CBCharacteristic *)characteristic
{
    [self.centrals removeObject:central];
    [self.readyCentrals removeObject:central];
    
    if (self.readyCentrals.count == 0) {
        [self.delegate bluetoothManagerDepotDisconnected];
    }
}

- (void)peripheralManagerDidStartAdvertising:(CBPeripheralManager *)peripheral error:(NSError *)error
{
    const char *response = "HeyHo!";
    self.characteristic.value = [NSData dataWithBytes:response length:strlen(response)];
}

static const char *handshakeCommand = "handshake";
static const char *depositCommand = "deposit";
static const char *timeoutCommand = "timeout";
static const char *thumbsUpCommand = "Thumbs Up!";
static const char *disconnectCommand = "disconnect";
static const char *closeCommand = "close";

- (void)peripheralManager:(CBPeripheralManager *)peripheral didReceiveWriteRequests:(NSArray<CBATTRequest *> *)requests
{
    for (CBATTRequest *request in requests) {
        if ([request.characteristic.UUID isEqual:[CBUUID UUIDWithString:TRANSFER_CHARACTERISTIC_UUID]]) {
            if ([request.value isEqualToData:[NSData dataWithBytes:handshakeCommand length:strlen(handshakeCommand)]]) {
                [peripheral respondToRequest:request withResult:CBATTErrorSuccess];
                
                [peripheral updateValue:[NSData dataWithBytes:thumbsUpCommand length:strlen(thumbsUpCommand)] forCharacteristic:self.characteristic onSubscribedCentrals:self.centrals];
                
                [self.readyCentrals addObject:request.central];
                
                [self.delegate bluetoothManagerDepotConnected];
            } else if ([request.value isEqualToData:[NSData dataWithBytes:depositCommand length:strlen(depositCommand)]]) {
                [peripheral respondToRequest:request withResult:CBATTErrorSuccess];
                
                [self.delegate bluetoothManagerItemWasDeposited];
            } else if ([request.value isEqualToData:[NSData dataWithBytes:timeoutCommand length:strlen(timeoutCommand)]]) {
                [peripheral respondToRequest:request withResult:CBATTErrorSuccess];
                [self.delegate bluetoothManagerDepositTimedOut];
            } else {
                [peripheral respondToRequest:request withResult:CBATTErrorRequestNotSupported];
            }
        } else {
            [peripheral respondToRequest:request withResult:CBATTErrorWriteNotPermitted];
        }
    }
}

- (void)peripheralManager:(CBPeripheralManager *)peripheral didReceiveReadRequest:(CBATTRequest *)request
{
    const char *response = "HeyHo!";
    request.value = [NSData dataWithBytes:response length:strlen(response)];
    
    [peripheral respondToRequest:request withResult:CBATTErrorSuccess];
}

- (void)openBinType:(NSString *)binType
{
    if ([self.readyCentrals count] > 0)
    {
        NSString *commandString = [NSString stringWithFormat:@"open %@", binType];
        const char *szCommandString = [commandString cStringUsingEncoding:NSUTF8StringEncoding];
        [self.peripheralManager updateValue:[NSData dataWithBytes:szCommandString length:strlen(szCommandString)] forCharacteristic:self.characteristic onSubscribedCentrals:self.readyCentrals];
    }
}
@end
