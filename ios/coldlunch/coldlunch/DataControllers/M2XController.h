//
//  M2XController.h
//  coldlunch
//
//  Created by Daryl at Finger Foods on 2016-01-02.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "M2X.h"
#import "M2XStreamModel.h"

typedef void(^ModelsRetrievedCompletionBlock) (NSError *error);
typedef void(^ValueUpdatedCompletionBlock) (NSError *error);

static NSString *kM2XLasVegasDepotId = @"914f497ac671440773d85bf869fdfec3";
static NSString *kM2XUserPhoneDeviceId = @"b4f3eed20c9194bb56a90ac32b3ae112";

static NSString *kM2XGlassType = @"glass";
static NSString *kM2XPlasticType = @"plastic";
static NSString *kM2XPaper = @"paper";
static NSString *kM2XMetal = @"metal";

@interface M2XController : NSObject
@property (readonly, nonatomic) M2XClient *client;

@property (strong, nonatomic) M2XStreamModel *glassModel;
@property (strong, nonatomic) M2XStreamModel *plasticModel;
@property (strong, nonatomic) M2XStreamModel *metalModel;
@property (strong, nonatomic) M2XStreamModel *paperModel;


+ (instancetype)sharedInstance;

- (void)populateModelsWithCompletionHandler:(ModelsRetrievedCompletionBlock)completion;
- (int)getNormalizedValueFromTotal:(int)total againstMaximum:(int)maximum atLevel:(int)level; 
- (int)calculateTotalsFromModels;
- (void)updateStream:(M2XStream *)stream withNewValue:(float)value andCompletionHandler:(ValueUpdatedCompletionBlock)completion;
@end
