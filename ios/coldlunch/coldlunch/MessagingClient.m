//
//  MessagingClient.m
//  coldlunch
//
//  Created by Evan Lewis on 1/2/16.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import "MessagingClient.h"

@implementation MessagingClient

NSString* const AppKey= @"";
NSString* const AppSecret= @"";
NSString* const AppScope = @"";
NSString* const BaseURL = @"";
NSString* const RedirectURL = @"";

- (void) setupClient {
    _appConfig = [[ATTAppConfiguration alloc] init];
    _appConfig.key = AppKey;
    _appConfig.secret = AppSecret;
    _appConfig.scope = AppScope;
    _appConfig.baseURL = [NSURL URLWithString:BaseURL];
    _appConfig.redirectURL = [NSURL URLWithString:RedirectURL];
    
    _messagingManager = [[IAMManager alloc] initWithConfig:_appConfig andDelegate:nil];
    
}

- (void) sendSMSTo:(NSString *)phoneNumber withMessage:(NSString *)message {
    [_messagingManager sendSMS:message to:phoneNumber failure:^(NSError *error) {
        NSLog(@"An error occurred sending the SMS: %@", error.localizedDescription);
    }];
}

@end
