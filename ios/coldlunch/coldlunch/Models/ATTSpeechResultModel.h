//
//  ATTSpeechResultModel.h
//  coldlunch
//
//  Created by Jordan Hesse on 2016-01-02.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface ATTSpeechAnalysisModel : JSONModel

@property (strong, nonatomic) NSString *ResultText;

@end

@protocol ATTSpeechAnalysisModel <NSObject>

@end

@interface ATTSpeechRecognitionModel : JSONModel

@property (strong, nonatomic) NSArray<ATTSpeechAnalysisModel, Optional> *NBest;

@end

@interface ATTSpeechResultModel : JSONModel

@property (strong, nonatomic) ATTSpeechRecognitionModel *Recognition;

@end
