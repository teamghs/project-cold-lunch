//
//  ATTOAuthModel.h
//  coldlunch
//
//  Created by Jordan Hesse on 2016-01-02.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import "ATTBaseModel.h"

@interface ATTOAuthModel : ATTBaseModel

@property (strong, nonatomic) NSString *accessToken;
@property (strong, nonatomic) NSString *expiresIn;
@property (strong, nonatomic) NSString *refreshToken;
@property (strong, nonatomic) NSString *tokenType;

@end
