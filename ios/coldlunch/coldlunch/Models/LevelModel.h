//
//  LevelModel.h
//  coldlunch
//
//  Created by Evan Lewis on 1/2/16.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import "JSONModel.h"

@interface LevelModel : NSObject

@property (nonatomic) int levelUpMax;
@property (nonatomic) int neighborHoodLevelUpMax;
@property (nonatomic) int cityLevelUpMax;

@property (nonatomic) int itemMax;

@end
