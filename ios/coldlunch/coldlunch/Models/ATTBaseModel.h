//
//  ATTBaseModel.h
//  coldlunch
//
//  Created by Jordan Hesse on 2016-01-02.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface ATTBaseModel : JSONModel

@end
