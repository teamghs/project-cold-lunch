//
//  M2XStreamModel.h
//  coldlunch
//
//  Created by Evan Lewis on 1/2/16.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import "ATTBaseModel.h"
#import "M2XStream.h"

@interface M2XStreamModel : ATTBaseModel
{
    @public M2XStream* stream;
}

@property (strong, nonatomic) NSString *created;
@property (strong, nonatomic) NSString *displayName;
@property (strong, nonatomic) NSString *latestValueAt;
@property (strong, nonatomic) NSDictionary *unit;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *updated;
@property (strong, nonatomic) NSString *url;
@property (strong, nonatomic) NSString *value;

@end
