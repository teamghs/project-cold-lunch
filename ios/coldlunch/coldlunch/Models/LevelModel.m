//
//  LevelModel.m
//  coldlunch
//
//  Created by Evan Lewis on 1/2/16.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import "LevelModel.h"

@implementation LevelModel

-(instancetype)init {
    if (self = [super init]) {
        self.itemMax = 80;
        
        self.levelUpMax = 320;
        self.cityLevelUpMax = 5000;
        self.neighborHoodLevelUpMax = 1600;
    }
    return self;
}

@end
