//
//  MessagingClient.h
//  coldlunch
//
//  Created by Evan Lewis on 1/2/16.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InAppMessaging/ATTAppConfiguration.h"
#import "InAppMessaging/IAMManager.h"

@interface MessagingClient : NSObject

@property (nonatomic, strong) ATTAppConfiguration *appConfig;
@property (nonatomic, strong) IAMManager *messagingManager;


- (void) setupClient;
- (void) sendSMSTo:(NSString *)phoneNumber withMessage:(NSString *)message;

@end
