//
//  UIView+ProgressRing.m
//  coldlunch
//
//  Created by Jordan Hesse on 2016-01-02.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import "UIView+ProgressRing.h"

@implementation UIView (ProgressRing)

- (void)animateProgress:(CGFloat)progress color:(UIColor *)color
              lineWidth:(CGFloat)lineWidth inset:(CGFloat)inset
               duration:(NSTimeInterval)duration {
    CAShapeLayer *circle = [CAShapeLayer layer];
    [circle setPath:[UIBezierPath bezierPathWithArcCenter:CGPointMake(CGRectGetMidX(self.bounds),
                                                                      CGRectGetMidY(self.bounds))
                                                   radius:(CGRectGetMidX(self.bounds) - inset)
                                               startAngle:-M_PI_2
                                                 endAngle:-M_PI_2 - (2.0 * M_PI * progress)
                                                clockwise:NO].CGPath];
    [circle setFillColor:[UIColor clearColor].CGColor];
    [circle setStrokeColor:color.CGColor];
    [circle setLineWidth:lineWidth];
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    [animation setDuration:duration];
    [animation setRemovedOnCompletion:NO];
    [animation setFromValue:@(0.0)];
    [animation setToValue:@(1.0)];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [circle addAnimation:animation forKey:@"drawCircleAnimation"];
    
    [self.layer.sublayers makeObjectsPerformSelector:@selector(removeFromSuperlayer)];
    [self.layer addSublayer:circle];
}

@end
