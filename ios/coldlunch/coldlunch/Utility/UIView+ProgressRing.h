//
//  UIView+ProgressRing.h
//  coldlunch
//
//  Created by Jordan Hesse on 2016-01-02.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (ProgressRing)

- (void)animateProgress:(CGFloat)progress color:(UIColor *)color
              lineWidth:(CGFloat)lineWidth inset:(CGFloat)inset
               duration:(NSTimeInterval)duration;

@end
