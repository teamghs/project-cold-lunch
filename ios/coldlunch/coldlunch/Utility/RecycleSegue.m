//
//  RecycleSegue.m
//  coldlunch
//
//  Created by Jordan Hesse on 2016-01-02.
//  Copyright © 2016 Team GHS. All rights reserved.
//

#import "RecycleSegue.h"
#import "RecycleViewController.h"

@implementation RecycleSegue

- (void)perform {
    id parentViewController = self.sourceViewController.parentViewController;
    if ([parentViewController isKindOfClass:[RecycleViewController class]]) {
        [parentViewController transitionToSubViewController:self.destinationViewController];
    }
}

@end
