from datetime import datetime, timedelta
import random
import json

end_date = datetime.now()
start_date = end_date - timedelta(weeks=1)

add_time_min = 30
add_time_max = 60

inc_min = 0.5
inc_max = 2

open_time = 0
close_time = 23

current_date = start_date

data_points = []
last_value = 0

while current_date <= end_date:
    end_time = datetime(current_date.year, current_date.month, current_date.day, close_time, 0, 0)
    current_time = datetime(current_date.year, current_date.month, current_date.day, open_time, 0, 0)

    while current_time <= end_time:
        data_points.append({
            "timestamp": current_time.isoformat(),
            "value": last_value
        })

        last_value += random.uniform(inc_min, inc_max)
        current_time += timedelta(minutes=random.uniform(add_time_min, add_time_max))

    current_date = current_date + timedelta(days=1)

def chunks(l, n):
    for i in xrange(0, len(l), n):
        yield l[i:i+n]

for chunk in chunks(data_points, 1000):
    body = {"values":chunk}

    print json.dumps(body)